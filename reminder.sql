-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 15, 2018 at 08:48 PM
-- Server version: 5.5.20
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `reminder_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_reminder`
--

CREATE TABLE IF NOT EXISTS `table_reminder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` varchar(64) NOT NULL,
  `description` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `table_reminder`
--

INSERT INTO `table_reminder` (`id`, `task`, `description`, `date`, `time`) VALUES
(10, 'Birthday', 'Malu', '2019-05-30', '12:00:00'),
(11, 'Meeting', 'Place : Trivandrum', '2019-04-30', '09:30:00'),
(12, 'Wedding', 'Kottayam', '2019-08-28', '11:00:00'),
(13, 'Tour', 'GOA', '2019-12-12', '06:00:00'),
(14, 'Seminar', 'Cochi', '2019-04-13', '10:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reminder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Amala Rani Sebastian
 */
public class Operationclass {
    
    Connectionclass connection = new Connectionclass();//Database Connection class object initialization

    public Operationclass() {
        this.con = connection.ConnectDB();
    }
    String query = "";
    Connection con;//Database Connection object declaration
    PreparedStatement  preparedstatement;   
    ResultSet resultset;
    
    public ResultSet viewreminders(){
        try{
        query = "select * from table_reminder";
        preparedstatement = con.prepareStatement(query);
        resultset = preparedstatement.executeQuery();
        }
        catch(Exception e){
            System.err.println("Exception :"+e.getMessage());  
        }
       return  resultset;
    }

    public void addreminder(String subject, String desc, String date, String time) {
        try{
       query = "insert into table_reminder (task,description,date,time)values(?,?,?,?)";
       preparedstatement = con.prepareStatement(query);
       preparedstatement.setString(1, subject);
       preparedstatement.setString(2,desc);
       preparedstatement.setString(3, date);
       preparedstatement.setString(4, time);
       preparedstatement.executeUpdate();
        }
        catch(Exception e){
            System.err.println("Exception :"+e.getMessage());
        }
    }

    public void editreminder(int ID, String subject, String desc, String date, String time) {
        query = "update table_reminder set task=?,description=?,date=?,time=? where id=?";
        try{
                preparedstatement = con.prepareStatement(query);
                preparedstatement.setString(1, subject);
                preparedstatement.setString(2,desc);
                preparedstatement.setString(3, date);
                preparedstatement.setString(4, time);
                preparedstatement.setInt(5, ID);
                preparedstatement.executeUpdate();
        }
        catch(Exception e){
            System.err.println("Exception :"+e.getMessage());
        }
    }

    public ResultSet getReminder(int ID) {
       query = "select * from table_reminder where id=?";
       try{
            preparedstatement = con.prepareStatement(query);
            preparedstatement.setInt(1, ID);
            resultset = preparedstatement.executeQuery();
           
       }
       catch(Exception e){
           
           System.out.println("Exception   :"+e.getMessage());  
       }
       return  resultset;
    }
     
}
